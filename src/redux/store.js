import {configureStore} from "@reduxjs/toolkit"
import userReducer from "./user"
import invoiceReducer from "./invoice"
import selectedProductReducer from "./selectedProduct";
import selectedCustomerReducer from "./selectCustomer";
export const store = configureStore({
    reducer: {
      user: userReducer,
      invoice: invoiceReducer ,
      selectProduct: selectedProductReducer,
      selectCustomer: selectedCustomerReducer
    }
})