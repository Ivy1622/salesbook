import {createSlice} from "@reduxjs/toolkit";
import selectCustomer from "../Pages/SelectCustomer.jsx";
export const selectcustomerSlice = createSlice({
  name: "selectcustomer",
  initialState: {
    customers: []
  },
  reducers: {
    setSelectedCustomers: (state , action) => {
      state.customers = action.payload
    }

  }

})


export const { setSelectedCustomers} = selectcustomerSlice.actions
export default selectcustomerSlice.reducer