import {createSlice} from "@reduxjs/toolkit";
import selectProducts from "../Pages/SelectProducts.jsx";

export const selectproductSlice = createSlice({
  name: "selectproduct",
  initialState: {
    productlist: []
  },
  reducers: {
    setSelectedProducts: (state , action) => {
      state.products = action.payload
    }

  }

})


export const { setSelectedProducts} = selectproductSlice.actions
export default selectproductSlice.reducer