// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from 'firebase/firestore'; //for access to Firestore
import { EmailAuthProvider } from 'firebase/auth'; //for email and password authentication
import { getAuth } from 'firebase/auth'; // for access to authentication
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAxRmlfwMGU69gf1eLw-Kq-vXtgywPaGck",
  authDomain: "salesbook-af42d.firebaseapp.com",
  projectId: "salesbook-af42d",
  storageBucket: "salesbook-af42d.appspot.com",
  messagingSenderId: "837242680954",
  appId: "1:837242680954:web:9fbdb37f215f21b0306dc3",
  measurementId: "G-CCF1V0NLF3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const provider = new EmailAuthProvider();
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);
export { provider, auth, storage, db };
