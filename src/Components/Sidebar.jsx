import React from 'react';
import {BsBoxArrowRight, BsPeople , BsCreditCard2Back, BsCart4, BsCashCoin, BsClipboardPulse} from "react-icons/bs";
import styled from 'styled-components';
import logo from '../images/logo2.png';
import { UserAuth } from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';


const Container = styled.div`
  position: fixed;
  .active {
    border-right: 4px solid var(--white);
    img {
      filter: invert(100%) sepia(0%) saturate(0%) hue-rotate(93deg)
        brightness(103%) contrast(103%);
    }
  }
`;

const Button = styled.button`
  background-color: var(--bleue);
  border: none;
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 50%;
  margin: 0.5rem 0 0 0.5rem;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  &::before,
  &::after {
    content: "";
    background-color: var(--white);
    height: 2px;
    width: 1rem;
    position: absolute;
    transition: all 0.3s ease;
  }
  &::before {
    top: ${(props) => (props.clicked ? "1.5" : "1rem")};
    transform: ${(props) => (props.clicked ? "rotate(135deg)" : "rotate(0)")};
  }
  &::after {
    top: ${(props) => (props.clicked ? "1.2" : "1.5rem")};
    transform: ${(props) => (props.clicked ? "rotate(-135deg)" : "rotate(0)")};
  }
`;

const SidebarContainer = styled.div`
  background-color: var(--bleue);
  width: 6rem;
  height: 80vh;
  margin-top: 1rem;
  border-radius: 0 30px 30px 0;
  padding: 1rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  position: relative;
`;

const Logo = styled.div`
  width: 100%;
  color:var(--white);
  svg {
    width: 100%;
    height: auto;
  }
`;

const SlickBar = styled.ul`
  color: var(--white);
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--bleue);
  padding: 2rem 0;
  position: absolute;
  top: 6rem;
  left: 0;
  width: ${(props) => (props.clicked ? "17rem" : "5.5rem")};
  transition: all 0.5s ease;
  border-radius: 0 30px 30px 0;
`;

const Item = styled.a`
  text-decoration: none;
  color: var(--white);
  width: 100%;
  padding: 1rem 0;
  cursor: pointer;
  display: flex;
  padding-left: 1rem;
  &:hover {
    border-right: 4px solid var(--white);
    svg {
      filter: invert(100%) sepia(0%) saturate(0%) hue-rotate(93deg)
        brightness(103%) contrast(103%);
    }
  }
  svg {
    width: 1.2rem;
    height: auto;
    filter: invert(92%) sepia(4%) saturate(1033%) hue-rotate(169deg)
      brightness(78%) contrast(85%);
  }
`;

const Text = styled.span`
  width: ${(props) => (props.clicked ? "100%" : "0")};
  overflow: hidden;
  margin-left: ${(props) => (props.clicked ? "1.5rem" : "0")};
  transition: all 0.3s ease;
`;
const Sidebar = () => {
  const [click, setClick] = React.useState(false);
  const handleClick = () => setClick(!click);
  const { logout } = UserAuth();

  const navigate = useNavigate();
  const handleLogout = async () => {
    try {
      await logout();
      navigate('/');
      console.log('Vous êtes déconnecté');
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <Container>
      <Button clicked={click} onClick={() => handleClick()}>

      </Button>
      <SidebarContainer>
        <Logo>
          <img src={logo} alt="Logo" className="img-fluid"  />
        </Logo>

        <SlickBar clicked={click}>
         <Item
            onClick={() => setClick(false)}
            exact
            activeClassName="active"
            href={"/user/selectCustomer"}
          >

            <BsCreditCard2Back style={{color:"dimgray"}}/>
             <Text clicked={click}>Nouvelle vente </Text>

          </Item>

          <Item
            onClick={() => setClick(false)}
            activeClassName="active"
            href={"/user/productList"}
          >
             <BsCart4 style={{color:"dimgray"}}/>
              <Text clicked={click}>Liste des produits</Text>
          </Item>

          <Item
            onClick={() => setClick(false)}
            activeClassName="active"
            href={"/user/customerList"}

          >
             <BsPeople style={{color:"dimgray"}}/>
              <Text clicked={click}>Liste des Clients</Text>
          </Item>

           <Item
            onClick={() => setClick(false)}
            activeClassName="active"
            to="/team"
          >
             <BsCashCoin style={{color:"dimgray"}}/>
              <Text clicked={click}>Toutes les dépenses</Text>
          </Item>

           <Item
            onClick={() => setClick(false)}
            activeClassName="active"
            to="/team"
          >
             <BsClipboardPulse style={{color:"dimgray"}}/>
              <Text clicked={click}>Consulter les rapports</Text>
          </Item>


        </SlickBar>
        <button onClick={handleLogout}
                className={"bg-white rounded-2 p-lg-2 shadow-lg d-flex justify-content-center"}>
            < BsBoxArrowRight className={""} style={{color:"red"}}/>
        </button>
      </SidebarContainer>

    </Container>
  );
};

export default Sidebar;