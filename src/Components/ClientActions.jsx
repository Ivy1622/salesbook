import { useNavigate } from 'react-router-dom';
import { doc, deleteDoc } from 'firebase/firestore';
import { db } from '../firebase';
import { showToast } from '../utils/functions';
import PropTypes from "prop-types";

const CustomerActionsSvg = ({ customerId, deleteCustomerlocal }) => {
  const navigate = useNavigate();


  async function deleteCustomer(customerId) {
    try {
      await deleteDoc(doc(db, 'customers', customerId));
      deleteCustomerlocal(customerId)
      showToast("success", 'Suppression réussie!🚀')
    } catch (error) {
        console.log(error)
      showToast("error", 'Echec, veuillez reéssayer!😭')
    }
  }

  return (
    <div className="d-flex justify-items-center">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="text-primary"
        height={20}
        fill="none"
        style={{cursor:"pointer"}}
        viewBox="0 0 24 24"
        stroke="currentColor"
        strokeWidth={2}
        onClick={() => navigate(`/user/modifiedCustomer/`+ customerId)}
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"
        />
      </svg>

      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="text-danger ms-3"
        height={20}
        fill="none"
        style={{cursor:"pointer"}}
        viewBox="0 0 24 24"
        stroke="currentColor"
        strokeWidth={2}
        onClick={() => deleteCustomer(customerId)}
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
        />
      </svg>
    </div>
  );
};

CustomerActionsSvg.propTypes = {
    customerId: PropTypes.string,
    deleteCustomerlocal: PropTypes.func
}

export default CustomerActionsSvg;
