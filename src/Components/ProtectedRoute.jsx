import { UserAuth } from '../context/AuthContext'
import { Navigate } from 'react-router-dom'

// eslint-disable-next-line react/prop-types
const ProtectedRoute = ({ children }) => {
    const { user } = UserAuth()

    if (!user) {
        return <Navigate to='/' />
    }
    return children
}

export default ProtectedRoute