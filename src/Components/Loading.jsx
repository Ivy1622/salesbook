import React from 'react'
import RingLoader from "react-spinners/RingLoader";

const Loading = () => {
  return (
    <main className="bg-light d-flex align-items-center justify-content-center" style={{height:"1029px"}} >
        <RingLoader/>
    </main>
  )
}

export default Loading