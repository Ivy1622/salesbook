import { Route, createBrowserRouter, createRoutesFromElements, RouterProvider } from 'react-router-dom';
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {AuthContextProvider} from "./context/AuthContext.jsx";
import ProtectedRoute from "./Components/ProtectedRoute.jsx";
import Emailstart from './Pages/Email';
import Register from './Pages/Register';
import Login from './Pages/Login';
import ResetPasswordForm from './Pages/ResetPassword';
import NewProduct from './Pages/NewProduct';
import ProductList from './Pages/ProductList';
import ModifiedProduct from './Pages/ModifiedPoduct.jsx';
import NewCustomer from "./Pages/NewCustomer.jsx";
import CustomersList from "./Pages/CustomersList.jsx";
import ModifiedCustomer from "./Pages/ModifiedCustomer.jsx";
import SelectCustomer from "./Pages/SelectCustomer.jsx";
import SelectProduct from "./Pages/SelectProducts.jsx";
import CreateInvoice from "./Pages/SalesValidation.jsx";
import Invoice from "./Pages/Invoice.jsx";

const router = createBrowserRouter(
  createRoutesFromElements(

    <Route>
      <Route exact path="/" element={<Emailstart />} />
      <Route path="register/:email" element={<Register />} />
      <Route path="login/:email" element={<Login />} />
      <Route path="resetpassword/:email" element={<ResetPasswordForm />} />
      
      <Route path="user/newProduct" element={<ProtectedRoute><NewProduct /></ProtectedRoute>} />
      <Route path="user/productList" element={<ProtectedRoute><ProductList /></ProtectedRoute>} />
      <Route path="user/modifiedProduct/:produtId" element={<ProtectedRoute><ModifiedProduct /></ProtectedRoute>} />
      <Route path="user/newcustomer" element={<ProtectedRoute><NewCustomer /></ProtectedRoute>} />
      <Route path="user/customerList" element={<ProtectedRoute><CustomersList /></ProtectedRoute>} />
      <Route path="user/modifiedCustomer/:customerId" element={<ProtectedRoute><ModifiedCustomer /></ProtectedRoute>} />
      <Route path="user/selectCustomer" element={<ProtectedRoute><SelectCustomer /></ProtectedRoute>} />
      <Route path="user/selectProduct" element={<ProtectedRoute><SelectProduct /></ProtectedRoute>} />
      <Route path="user/createInvoice" element={<ProtectedRoute> <CreateInvoice/> </ProtectedRoute>} />
      <Route path="user/invoice/:invoiceId" element={<ProtectedRoute><Invoice/></ProtectedRoute>} />
      
    </Route>

  )
)

function App({routes}) {

  return (
    <>
     <AuthContextProvider>
      <RouterProvider router={router}/>
      <ToastContainer />
    </AuthContextProvider>
    </>
  );
}

export default App;
