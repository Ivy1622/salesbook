import { useState } from 'react';
import logo from '../images/logo.png';
import {BsPerson, BsPhone} from "react-icons/bs";
import { db } from '../firebase';
import { collection, addDoc } from "firebase/firestore";
import Loading from '../components/Loading';
import { showToast } from '../utils/functions';
import Sidebar from "../Components/Sidebar.jsx";
import { UserAuth } from '../context/AuthContext';
import 'react-circular-progressbar/dist/styles.css';

const NewCustomer = () => {
    const { user } = UserAuth();
    const [customerName, setCustomerName] = useState('');
    const [customerPhone, setCustomerPhone] = useState('');
    const [loading, setLoading] = useState(false);
const percentage = 66;


    const handleNewCustomer = async (e) => {
        e.preventDefault();

        const customerRef = await addDoc(collection(db, "customers"),
            {
                user_id: user.uid,
                customerName,
                customerPhone,
            });

        setCustomerName('');
        setCustomerPhone('');

        showToast('success', 'Client ajouté avec succès');

    };

    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <main className="d-flex justify-content-center">
                    <div className='col-lg-3'>
                        <form
                            className="pb-5 "
                            onSubmit={handleNewCustomer}
                        >
                            <div className="">
                                <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
                            </div>
                            <div className="pb-5">
                                <p className='fs-1' style={{ color: "#4F378B" }}> Nouveau Client</p>
                            </div>

                            <label htmlFor="text" className="mb-2" style={{ color: "#6750A4" }}>
                                Nom du client
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsPerson /></i>
                                    <input
                                        type="text"
                                        autoComplete='off'
                                        required
                                        className="form-control p-3
                                        mb-4 border rounded"
                                        placeholder="Entrez le nom du client"
                                        value={customerName}
                                        onChange={(e) => setCustomerName(e.target.value)}
                                    /> 
                                </div>
                            </div>

                            <label htmlFor="phone" className="mb-2" style={{ color: "#6750A4" }}>
                                Numéro de téléphone
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 text-secondary'> < BsPhone/> </i>
                                    <input
                                        autoComplete='off'
                                        placeholder='Entrez le numéro ex: 655443322'
                                        type="tel"
                                        pattern="^6[0-9]{8}"
                                        className="form-control mb-4 p-3 rounded "
                                        required
                                        value={customerPhone}
                                        onChange={(e) => setCustomerPhone(e.target.value)}
                                    />
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="col-lg-12 p-3 text-white btn btn-lg mt-5 " style={{ backgroundColor: "#1E1A76" }}>
                                Ajouter
                            </button>

                        </form>
                    </div>
                </main>
            )}

        </>
    );
};

export default NewCustomer;