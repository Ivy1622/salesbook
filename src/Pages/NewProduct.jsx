import React, { useState } from 'react';
import logo from '../images/logo.png';
import { Bs0Square } from "react-icons/bs";
import { db } from '../firebase';
import { collection, addDoc } from "firebase/firestore";
import Loading from '../components/Loading';
import { showToast } from '../utils/functions';
import { UserAuth } from '../context/AuthContext';
import Sidebar from "../Components/Sidebar.jsx";


const NewProduct = () => {
    const { user } = UserAuth();
    const [price, setPrice] = useState(0);
    const [productName, setProductName] = useState('');
    const [loading, setLoading] = useState(false);
    const handleNewProduct = async (e) => {
        e.preventDefault();

        const productRef = await addDoc(collection(db, "products"),
            {
                user_id: user.uid,
                price,
                productName,

            });

        setPrice('');
        setProductName('');
        
        showToast('success', 'Produit ajouté avec succès');

    };

    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <main className="d-flex justify-content-center">
                    <div className='col-lg-3'>
                        <form
                            className="pb-5 "
                            onSubmit={handleNewProduct}
                        >
                            <div className="">
                                <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
                            </div>
                            <p>User Email: {user && user.email}</p>
                            <p>User Name: {user && user.name}</p>

                            <div className="pb-5">
                                <p className='fs-1' style={{ color: "#4F378B" }}> Nouveau Produit</p>
                            </div>

                            <label htmlFor="text" className="mb-2" style={{ color: "#6750A4" }}>
                                Nom du produit
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <Bs0Square /></i>
                                    <input
                                        type="text"
                                        id="productName"
                                        autoComplete='off'
                                        required
                                        className="form-control p-3 
                                        mb-4 border rounded"
                                        placeholder="Entrez le nom du produit"
                                        value={productName}
                                        onChange={(e) => setProductName(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="tel" className="mb-2" style={{ color: "#6750A4" }}>
                                Prix
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 text-secondary'> (CFA) </i>
                                    <input
                                        autoComplete='off'
                                        placeholder='Entrez le prix du produit'
                                        id="price"
                                        type="number"
                                        className="form-control mb-4 p-3 rounded "
                                        required
                                        value={price}
                                        onChange={(e) => setPrice(e.target.value)}
                                    />
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="col-lg-12 p-3 text-white btn btn-lg mt-5 " style={{ backgroundColor: "#1E1A76" }}>
                                Ajouter
                            </button>

                        </form>
                    </div>
                </main>
            )}
            <a href='/user/productList' className='d-flex justify-content-center fst-italic' style={{ textDecoration: "none", color: "#6750A4" }}> Voir la liste des produits</a>

        </>
    );
};

export default NewProduct; 