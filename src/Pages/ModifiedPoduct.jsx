import React, { useEffect, useState } from 'react';
import logo from '../images/logo.png';
import { Bs0Square } from "react-icons/bs";
import { db } from '../firebase';
import Loading from '../components/Loading';
import { doc, updateDoc, getDoc, query } from 'firebase/firestore';
import { useParams } from 'react-router-dom';
import { showToast } from '../utils/functions';
import { useNavigate } from 'react-router-dom';
import Sidebar from "../Components/Sidebar.jsx";


const ModifiedProduct = () => {
    const { produtId } = useParams();
    const [loading, setLoading] = useState(false);
    const [productName, setProductName] = useState('');
    const [price, setPrice] = useState('');
    const navigate = useNavigate();

    const handleUpdate = async (e) => {
        e.preventDefault()
        const productRef = doc(db, "products", produtId)

        try {
            await updateDoc(productRef, { productName, price });
            showToast('success', 'Produit a été mis à jour avec succès');
            navigate('/user/productList')
        } catch (error) {
            showToast('error', 'Une erreur s\'est produite lors de la mise à jour du produit ');
        }
    };

    const getProductsById = async (produtId) => {
        const p = query(doc(db, "products", produtId))
        const querySnapshot = await getDoc(p);
        const productData = querySnapshot.data();
        console.log(productData)
        setProductName(productData.productName)
        setPrice(productData.price)
    };

    useEffect(() => {

        getProductsById(produtId)

    }, [])

    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <main className="d-flex justify-content-center">
                    <div className='col-lg-3'>
                        <form
                            className="pb-5 "
                            onSubmit={handleUpdate}
                        >
                            <div className="">
                                <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
                            </div>
                            <label htmlFor="text" className="mb-2" style={{ color: "#6750A4" }}>
                                Nom du produit
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <Bs0Square /></i>
                                    <input
                                        type="text"
                                        id="productName"
                                        autoComplete='off'
                                        required
                                        className="form-control p-3 
                                        mb-4 border rounded"
                                        placeholder="Entrez le nom du produit"
                                        value={productName}
                                        onChange={(e) => setProductName(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="tel" className="mb-2" style={{ color: "#6750A4" }}>
                                Prix
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 text-secondary'> (CFA) </i>
                                    <input
                                        autoComplete='off'
                                        placeholder='Entrez le prix du produit'
                                        id="price"
                                        type="number"
                                        className="form-control mb-4 p-3 rounded "
                                        required
                                        value={price}
                                        onChange={(e) => setPrice(e.target.value)}
                                    />
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="col-lg-12 p-3 text-white btn btn-lg mt-5 " style={{ backgroundColor: "#1E1A76" }} >
                                Modifier
                            </button>

                        </form>
                    </div>
                </main>
            )}
            <a href='/user/productList' className='d-flex justify-content-center fst-italic' style={{ textDecoration: "none", color: "#6750A4" }}> Voir la liste des produits</a>

        </>
    );
};

export default ModifiedProduct; 