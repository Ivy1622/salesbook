import { useEffect, useState } from 'react';
import {collection, getDocs, query, where} from "firebase/firestore";
import { db } from '../firebase';
import Loading from '../components/Loading';
import CustomerActionsSvg from '../Components/ClientActions.jsx';
import Sidebar from '../Components/Sidebar.jsx'
import {UserAuth} from "../context/AuthContext.jsx";

const CustomersList = () => {
    const [customers, setCustomers] = useState([])
    const {user} = UserAuth();
    const [loading, setLoading] = useState(true

    );

    const getCustomers = async () => {
        const c = query(collection(db, "customers"), where('user_id', '==', user.uid))
        const querySnapshot = await getDocs(c);
        const data = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))

        console.log(data)
        setCustomers(data)
        setLoading(false)
    };

    useEffect(() => {

        getCustomers()

    }, [])

    const deleteCustomer = (customerId) => {
        const tab = customers.filter(customer => customer.id !== customerId)
        setCustomers(tab)
        console.log(tab)
    }
 ;


    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <div>

                    <div className={"py-3"}></div>
                <div className='col-lg-6 offset-lg-3 py-5 ' >
                    <h5 className='pt-5 pb-4 mx-5 row' style={{ color: "#071108" }}> Liste des clients enregistrés </h5>
                    <table className="table table-hover mx-5">
                        <thead>
                            <tr >
                                <th scope="col" style={{ color: "#0F056B" }} >Nom du client</th>
                                <th scope="col" style={{ color: "#0F056B" }}>Numéro de téléphone</th>
                                <th scope="col" style={{ color: "#0F056B" }}>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            {customers.map((customer) => (
                                <tr key={customer.id}>
                                    <td >{customer.customerName}</td>
                                    <td>{customer.customerPhone}</td>
                                    <td>
                                        <CustomerActionsSvg deleteCustomerlocal={deleteCustomer} customerId={customer.id} />
                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                </div>
                    </div>
            )}
        </>
    );
};

export default CustomersList;