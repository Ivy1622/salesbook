import React, { useState } from 'react';
import '../App.css';
import logo from '../images/logo.png';
import { BsLock } from "react-icons/bs";
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { showToast } from '../utils/functions';
import Loading from '../components/Loading';
import {UserAuth} from "../context/AuthContext.jsx";

const Login = () => {
  const navigate = useNavigate();
  const { email } = useParams();
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const { signIn } = UserAuth();

  const handleSignIn = async (e) => {
    e.preventDefault();
    setLoading(true);
    setError('');
    try {
      await signIn(email, password);
      setLoading(false);
      showToast('success', 'Connexion réussie 🚀');
      navigate("/user/newProduct");
      } catch (error) {
      setError(error.message);
      setLoading(false);
      showToast('error', 'Erreur d\'authentification 😭 ');
      console.log(error.message);
    }
  };

  return (
    <>
    {loading ? (
        <Loading />
      ) : (
    <main className="">
      <div className="d-flex justify-content-center">
        <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
      </div>
      <div className="d-flex justify-content-center">
        <p className='fs-1' style={{ color: "#4F378B" }}> Connexion</p>
      </div>
      <main className="d-flex justify-content-center container-fluid py-5">

        <form
          className="p-5 col-lg-3 card"
          onSubmit={handleSignIn}
        >


          <label htmlFor="email" className="mb-2 fw-bold" style={{ color: "#6750A4" }}>
            Mot de passe
          </label>
          <div className="input col-lg">
            <i className='p-3 '> <BsLock /></i>
            <input
              id="password"
              type="password"
              placeholder='Entrez votre mot de passe'
              className="col-lg-12 mb-5 border p-3 rounded "
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>


          <button
            type="submit"
            className="col-lg-12 p-3  text-white btn btn-lg " style={{ backgroundColor: "#1E1A76" }}>
            Connexion
          </button>
          <div>
          </div>
        </form>

      </main>
      <p className='text-center'>Mot de passe oublié
        <a href="/resetpassword/ + 'email' " style={{ textDecoration: "none" }}>
          <span className='fst-italic' style={{ color: "#381E72" }}> Réinitialisez-le</span></a></p>
      </main>
       )}
    </>
  );
};

export default Login;
