import { useEffect, useState } from 'react';
import {collection, getDocs, query, where} from "firebase/firestore";
import { db } from '../firebase';
import Loading from '../components/Loading';
import ProductActionsSvg from '../Components/ProductActions';
import Sidebar from "../Components/Sidebar.jsx";
import {UserAuth} from "../context/AuthContext.jsx";

const ProductList = () => {
    //const user = useSelector((state) => state.user.user);
    const [products, setProducts] = useState([])
    const {user} = UserAuth();
    console.log(user)
    const [loading, setLoading] = useState(true);

    const getProducts = async () => {
        const q = query(collection(db, "products"), where('user_id', '==', user.uid))
        const querySnapshot = await getDocs(q);
        const data = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))

        console.log(data)
        setProducts(data)
        setLoading(false)
    };

    useEffect(() => {

        getProducts()

    }, [])

    const deleteProduct = (produtId) => {
        /*let tab = new Array()
        for (var i=0; i < products.length; i++) {
            if (products[i].id !== produtId){
                tab.push(products[i])
            }
        } */
        const tab = products.filter(product => product.id !== produtId)
        setProducts(tab)
        console.log(tab)
    }

    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <div className='col-lg-6 offset-lg-3' >
                    <a href={'/user/newProduct'}
                    type="btn"
                    className="col-lg-3 offset-lg-9 p-3 text-white btn mt-5 " style={{ backgroundColor: "#1E1A76" }}>
                      + Nouveau Produit
                 </a>
                    <h5 className='pt-5 pb-4 mx-5 row' style={{ color: "#071108" }}> Listes des produits enregistrés </h5>
                    <table className="table table-hover mx-5">
                        <thead>
                            <tr >
                                <th scope="col" style={{ color: "#0F056B" }} >Nom du produit</th>
                                <th scope="col" style={{ color: "#0F056B" }}>Prix</th>
                                <th scope="col" style={{ color: "#0F056B" }}>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            {products.map((product) => (
                                <tr key={product.id}>

                                    <td >{product.productName}</td>
                                    <td>{product.price}</td>
                                    <td>
                                        <ProductActionsSvg deleteProductlocal={deleteProduct} produtId={product.id} />
                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                </div>
            )}
        </>
    );
};

export default ProductList; 