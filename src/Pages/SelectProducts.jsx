import { useEffect, useState } from 'react';
import { collection, getDocs, query } from "firebase/firestore";
import { db } from '../firebase';
import Loading from '../components/Loading';
import Sidebar from "../Components/Sidebar.jsx";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {setSelectedProducts} from "../redux/selectedProduct.js";

const SelectProduct = () => {

    const dispatch = useDispatch()
    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(true);
    const [selectedRows, setSelectedRows] = useState([]);
    const navigate = useNavigate()
    const getProducts = async () => {
        const q = query(collection(db, "products"))

        const querySnapshot = await getDocs(q);
        const data = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))

        setProducts(data)
        setLoading(false)
    };

    useEffect(() => {

        getProducts()

    }, [])
     const handleLigneClic = (produtId) => {
    const isAlreadySelected = selectedRows.includes(produtId);

    if (isAlreadySelected) {
      // Supprimer le client de la sélection
      setSelectedRows((prevSelectedProducts) =>
        prevSelectedProducts.filter((selectedPoducts) => selectedPoducts !== produtId)
      );
    } else {
      // Ajouter le client à la sélection
      setSelectedRows((prevSelectedProducts) => [...prevSelectedProducts, produtId]);
    }
  };

  // Fonction pour afficher les clients sélectionnés sur une autre page JSX
  const afficherSelection = () => {
    // Code pour naviguer vers une autre page JSX et afficher les clients sélectionnés
    // Utilisez la méthode de navigation appropriée pour votre configuration de routage
    const selectedProductsData = products.filter((product) => selectedRows.includes(product.id));
    dispatch(setSelectedProducts([...selectedProductsData]))
    navigate("/user/CreateInvoice")
    console.log(selectedProductsData); // Affichage des clients sélectionnés dans la console (à titre d'exemple)
  };


    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <div className='col-lg-6 offset-lg-3' >
                    <h5 className='pt-5 pb-4 mx-5 row' style={{ color: "#071108" }}> Listes des produits enregistrés </h5>
                    <table className="table table-hover mx-5">
                        <thead>
                            <tr >
                                <th scope="col" style={{ color: "#0F056B" }} >Nom du produit</th>
                                <th scope="col" style={{ color: "#0F056B" }}>Prix</th>
                            </tr>
                        </thead>
                        <tbody>

                            {products.map((product) => (
                                <tr key={product.id}
                                style={{border: selectedRows.includes(product.id) ? "2px solid #1E1A76 " : "1px  lightgray"}}
                                 onClick={() => handleLigneClic(product.id)}>

                                    <td >{product.productName}</td>
                                    <td>{product.price}</td>

                                </tr>
                            ))}

                        </tbody>
                        <button type="btn" onClick={afficherSelection} className="col-lg-5 p-3 text-white btn  mt-3 " style={{ backgroundColor: "#1E1A76" }}>Sélectionner</button>

                    </table>
                </div>
            )}
        </>
    );
};

export default SelectProduct;