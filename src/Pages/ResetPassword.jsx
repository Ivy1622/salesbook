import React, { useState } from 'react';
import { getAuth, sendPasswordResetEmail } from 'firebase/auth';
import '../App.css';
import logo from '../images/logo.png';
import psw from '../images/forgotpsw.svg';
import { useNavigate } from 'react-router-dom';

const ResetPasswordForm = () => {
  const [email, setEmail] = useState('');
  const navigate = useNavigate();

  const handleResetPassword = async (e) => {
    e.preventDefault();
    

    try {
      const auth = getAuth();
      sendPasswordResetEmail(auth, email)
        .then(() => {
          alert('Un lien de réinitialisation vous a été envoyé par e-mail.');
          navigate("/login/" + email);
        })

    } catch (error) {
      //alert('Une erreur s\'est produite. Veuillez réessayer.');
      console.log(error);
    }
  };

  return (

    <div>
      <main className="d-flex justify-content-center">

        <form
          className="pb-5 col-lg-3"
          onSubmit={handleResetPassword}
        >
          <div className="">
            <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
          </div>
          <div className="pb-5">
            <p className='fs-4' style={{ color: "#4F378B" }}> Mot de passe oublié ?</p>
          </div>
          <div className="pb-5">
            <img src={psw} alt="IMG" className="col-lg-11 img-fluid " />
          </div>
          <div className="">
            <p className='pb-4' style={{ color: "#4F378B" }}> Réinitialisez votre mot de passe</p>
          </div>
          <label htmlFor="email" className="mb-2 fw-bold" style={{ color: "#6750A4" }}>
            Email
          </label>
          <input
            autoComplete='off'
            placeholder='Entrez votre adresse Email'
            id="email"
            type="email"
            className="col-lg-12 mb-4 border p-3 rounded"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />


          <button
            type="submit"
            className="col-lg-12 p-3 text-white btn btn-lg " style={{ backgroundColor: "#1E1A76" }}>
            Réinitialiser
          </button>

        </form>
      </main>
    </div>
  );
};

export default ResetPasswordForm;