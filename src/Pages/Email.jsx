import { useState } from 'react';
import '../App.css';
import logo from '../images/logo.png';
import Email from '../images/email.svg';
import { db } from '../firebase';
import { useNavigate } from "react-router-dom";
import { collection, query, where, getDocs } from "firebase/firestore";
import Loading from '../components/Loading';


const Emailstart = () => {
  const [email, setEmail] = useState('');
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);


  const handleSubmit = async (e) => {
    e.preventDefault();
    
    await checkEmailExists(email);

  };

  const checkEmailExists = async (email) => {
    setLoading(true);
    try {
      const usersRef = query(collection(db, "users"), where("email", "==", email));
      const querySnapshot = await getDocs(usersRef);
      if (querySnapshot.empty) {
        setLoading(false);
        navigate("/register/" + email);
      } else {
        setLoading(false);
        navigate("/login/" + email);
      }
    } catch (error) {
      console.error('Erreur lors de la vérification de l\'e-mail:', error);
      throw error;
    }
  };

  return (
    <>
    {loading ? (
        <Loading />
      ) : (

    <main className="d-flex justify-content-center ">

      <form
        className="pb-5 col-lg-3"
        onSubmit={handleSubmit}
      >
        <div className="">
          <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
        </div>
        <div className="pb-5">
          <img src={Email} alt="IMG" className="col-lg-11 img-fluid" />
        </div>
        <label htmlFor="email" className="mb-2 fw-bold" style={{ color: "#6750A4" }}>
          Email
        </label>
        <input
          autoComplete='off'
          placeholder='Entrez votre adresse Email'
          id="email"
          type="email"
          className="col-lg-12 mb-4 border p-3 rounded"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />


        <button
          type="submit"
          className="col-lg-12 p-3 text-white btn btn-lg " style={{ backgroundColor: "#1E1A76" }}>
          Continuer
        </button>

      </form>
    </main>
    )}
    </>
  );
};

export default Emailstart;
