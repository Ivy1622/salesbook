import {useDispatch, useSelector} from 'react-redux';
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {addDoc, collection, doc, setDoc,serverTimestamp} from "firebase/firestore";
import { db } from '../firebase';
import user from "../redux/user.js";
import sidebar from "../Components/Sidebar.jsx";
import Sidebar from "../Components/Sidebar.jsx";
import {setInvoice} from "../redux/invoice.js";
import {showToast} from "../utils/functions.js";
import {UserAuth} from "../context/AuthContext.jsx";

const CreateInvoice =  () => {
    const {products} = useSelector(state => state.selectProduct);
    const [quantityProducts, setQuantityProducts] = useState([])
    const {customers} = useSelector(state => state.selectCustomer);
    const [mergedArray, setMergedArray] = useState([]);
    const navigate = useNavigate()
    const dispatch = useDispatch();
    const { user } = UserAuth();

    useEffect(() => {
        setQuantityProducts(Array(products.length).fill(1))
    }, [products.length]);

    const handleSubmit = (e) => {
        e.preventDefault();
        saveInvoice()
    }
    const saveInvoice = async (e) => {
    e.preventDefault();
    const merged = quantityProducts.map((quantity, index) => {
            return {
                ...products[index],
                quantity: quantity
            };
        });
    dispatch(
      setInvoice({
          productsMerged: merged,
          customers: customers ,
      })
    );

    addDoc(collection(db, 'invoices'), {
      user_id: user.uid,
      productsMerged: merged,
      customers: customers ,
      timestamp: serverTimestamp(),
    })
      .then((res) => {
        showToast('success', 'La facture a été crée avec succès!📜');
        console.log(res)
        navigate('/user/invoice/'+ res.id)
      })
      .catch((err) => {
        showToast('error', 'Essayez encore! Facture non crée!😭');
      });
  };

    return (
        <>
        <Sidebar/>
            <main className="d-flex justify-content-center ">
                <div>

                    {customers && customers.map((customer) => (
                    <div className={" pt-5 fs-4 text-secondary"} key={customer.id}>
                        Facture de  <span style={{color: "#6750A4"}}> {customer.customerName} </span>
                    </div>
                    ))}


                        <form onSubmit={handleSubmit}
>
                        {products && products.map((product, i) => (
                        <div className="pt-lg-4 " key={product.id}>
                            <div className={"row"}>
                                <div className={""}>
                                    <label htmlFor="text" className="mb-2" style={{color: "#6750A4"}}>
                                        Désignation:
                                    </label>
                                    <div>
                                        <div autoComplete='off'
                                             type={"text"}
                                             className="form-control mb-4 p-3 rounded input"
                                        >{product.productName}</div>
                                    </div>
                                </div>
                            </div>
                            <div className={"row"}>
                                <div className={"col-lg-4"}>
                                    <label htmlFor="tel" className="mb-2" style={{color: "#6750A4"}}>
                                        Qté:
                                    </label>
                                    <div className="input">
                                        <input autoComplete='off' type="number"
                                               className="form-control mb-4 p-3 rounded "
                                               required
                                               value={quantityProducts[i]}
                                               onChange={(e) => {
                                                   const value = +e.target.value
                                                   const newArray = quantityProducts.map((qty, idQty) => (idQty === i) ? value : qty)
                                                   setQuantityProducts(newArray)
                                               } }
                                        />
                                    </div>
                                </div>
                                <div className={"col-lg-4"}>
                                    <label htmlFor="tel" className="mb-2" style={{color: "#6750A4"}}>
                                        P.U:
                                    </label>
                                    <div className="input">
                                        <div autoComplete='off' type="number"
                                             className="form-control mb-4 p-3 rounded input"

                                             disabled
                                        >{product.price}</div>
                                    </div>
                                </div>

                                <div className={"col-lg-4"}>
                                    <label htmlFor="tel" className="mb-2" style={{color: "#6750A4"}}>
                                        Montant:
                                    </label>
                                    <div className="input">
                                        <input autoComplete='off' type="number"
                                               className="form-control mb-4 p-3 rounded "
                                               value={product.price * quantityProducts[i]}
                                               required
                                        />
                                    </div>
                                </div>

                            </div>

                        </div>
                            ))}
                        <button type="submit" className="col-lg-12 p-3 text-white btn btn-lg "
                            style={{backgroundColor: "#1E1A76"}} onClick={saveInvoice}> Valider
                        </button>
                        </form>
                </div>
            </main>
            )

        </>
    );

};
export default CreateInvoice;