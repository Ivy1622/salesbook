import { useEffect, useState } from 'react';
import { collection, getDocs, query } from "firebase/firestore";
import { db } from '../firebase';
import Loading from '../components/Loading';
import Sidebar from '../Components/Sidebar.jsx'
import {useNavigate} from "react-router-dom";
import {setSelectedCustomers} from "../redux/selectCustomer.js";
import {useDispatch} from "react-redux";

const SelectCustomer = () => {
    const [customers, setCustomers] = useState([])
    const [loading, setLoading] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]);
    const navigate = useNavigate()
    const dispatch = useDispatch()

  const getCustomers = async () => {
        setLoading(true)
        const c = query(collection(db, "customers"))

        const querySnapshot = await getDocs(c);
        const data = querySnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))

        console.log(data)
        setCustomers(data)
        setLoading(false)
    };

    useEffect(() => {

        getCustomers()

    }, [])

  // Fonction pour gérer le clic sur une ligne du tableau
  const handleLigneClic = (customerId) => {
    const isAlreadySelected = selectedRows.includes(customerId);
    if (isAlreadySelected) {
      // Supprimer le client de la sélection
      setSelectedRows((prevSelectedClients) =>
        prevSelectedClients.filter((selectedClient) => selectedClient !== customerId)
      );
    } else {
      // Ajouter le client à la sélection
      setSelectedRows((prevSelectedClients) => [prevSelectedClients, customerId]);
    }

  };

  // Fonction pour afficher les clients sélectionnés sur une autre page JSX
  const afficherSelection = () => {
    // Code pour naviguer vers une autre page JSX et afficher les clients sélectionnés
    // Utilisez la méthode de navigation appropriée pour votre configuration de routage
    const selectedClientsData = customers.filter((customer) => selectedRows.includes(customer.id));
    dispatch(setSelectedCustomers([...selectedClientsData]))
    navigate("/user/selectProduct")
    console.log(selectedClientsData); // Affichage des clients sélectionnés dans la console (à titre d'exemple)
  };

  return (
     <>
         <Sidebar />
         {loading ? (
             <Loading />
         ) : (
             <div>
                 <div className={"py-3"}></div>
                 <a href={'/user/newCustomer'}
                    type="btn"
                    className="col-lg-2 offset-lg-8 p-3 text-white btn mt-5 " style={{ backgroundColor: "#1E1A76" }}>
                      + Nouveau client
                 </a>
                 <div className='col-lg-7 offset-lg-3  ' >
                     <h5 className='pt-5 pb-4 mx-5 row' style={{ color: "#071108" }}> Liste des clients enregistrés </h5>
                     <table className="table table-hover mx-5">
                         <thead>
                         <tr >
                             <th scope="col" style={{ color: "#0F056B" }} >Nom du client</th>
                             <th scope="col" style={{ color: "#0F056B" }}>Numéro de téléphone</th>
                         </tr>
                         </thead>
                         <tbody>
                         {customers.map((customer) => (
                             <tr key={customer.id}
                                 style={{border: selectedRows.includes(customer.id) ? "2px solid #1E1A76 " : "1px  lightgray"}}
                                 onClick={() => handleLigneClic(customer.id)} >
                                 <td>{customer.customerName}</td>
                                 <td>{customer.customerPhone}</td>
                             </tr>
                         ))}
                         </tbody>
                         <button type="btn" onClick={afficherSelection} className="col-lg-5 p-3 text-white btn  mt-3 " style={{ backgroundColor: "#1E1A76" }}>Sélectionner</button>

                     </table>

                 </div>
             </div>
         )}
     </>

  );
}
export default SelectCustomer;
