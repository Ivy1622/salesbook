import React, { useEffect, useState } from 'react';
import logo from '../images/logo.png';
import { BsPerson, BsPhone} from "react-icons/bs";
import { db } from '../firebase';
import Loading from '../components/Loading';
import { doc, updateDoc, getDoc, query } from 'firebase/firestore';
import { useParams } from 'react-router-dom';
import { showToast } from '../utils/functions';
import { useNavigate } from 'react-router-dom';
import Sidebar from "../Components/Sidebar.jsx";

const ModifiedCustomer = () => {
    const { customerId } = useParams();
    const [loading, setLoading] = useState(false);
    const [customerName, setCustomerName] = useState('');
    const [customerPhone, setCustomerPhone] = useState('');
    const navigate = useNavigate();

    const CustomerUpdate = async (e) => {
        e.preventDefault()
        const customerRef = doc(db, "customers", customerId)

        try {
            await updateDoc(customerRef, { customerName, customerPhone });
            showToast('success', 'Le client a été mis à jour avec succès');
            navigate('/user/customerList')
        } catch (error) {
            showToast('error', 'Une erreur s\'est produite lors de la mise à jour du client ');
        }
    };

    const getCustomerById = async (customerId) => {
        const custom = query(doc(db, "customers", customerId))
        const querySnapshot = await getDoc(custom);
        const customerData = querySnapshot.data();
        console.log(customerData)
        setCustomerName(customerData.customerName)
        setCustomerPhone(customerData.customerPhone)
    };

    useEffect(() => {

        getCustomerById(customerId)

    }, [])

    return (
        <>
            <Sidebar />
            {loading ? (
                <Loading />
            ) : (
                <main className="d-flex justify-content-center">
                    <div className='col-lg-3'>
                        <form
                            className="pb-5 "
                            onSubmit={CustomerUpdate}
                        >
                            <div className="">
                                <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{ width: "200px" }} /> </a>
                            </div>
                            <label htmlFor="text" className="mb-2" style={{ color: "#6750A4" }}>
                                Nom du client
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsPerson /></i>
                                    <input
                                        type="text"
                                        autoComplete='off'
                                        required
                                        className="form-control p-3
                                        mb-4 border rounded"
                                        placeholder="Entrez le nom du client"
                                        value={customerName}
                                        onChange={(e) => setCustomerName(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="phone" className="mb-2" style={{ color: "#6750A4" }}>
                                Numéro de téléphone
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 text-secondary'> < BsPhone/> </i>
                                    <input
                                        autoComplete='off'
                                        placeholder='Entrez le numéro ex: 655443322'
                                        id="phone"
                                        type="tel"
                                        pattern="^6[0-9]{8}"
                                        className="form-control mb-4 p-3 rounded "
                                        required
                                        value={customerPhone}
                                        onChange={(e) => setCustomerPhone(e.target.value)}
                                    />
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="col-lg-12 p-3 text-white btn btn-lg mt-5 " style={{ backgroundColor: "#1E1A76" }}>
                                Modifier
                            </button>

                        </form>
                    </div>
                </main>
            )}
            <a href='/user/customerList' className='d-flex justify-content-center fst-italic' style={{ textDecoration: "none", color: "#6750A4" }}> Voir la liste des clients</a>

        </>
    );
};

export default ModifiedCustomer;