import {useSelector} from 'react-redux';
import {useEffect, useState} from "react";
import Sidebar from "../Components/Sidebar.jsx";
import {convertTimestamp} from "../utils/functions.js";
import {collection, doc, getDoc, getDocs, query, where} from "firebase/firestore";
import {db} from "../firebase.js";
import {UserAuth} from "../context/AuthContext.jsx";
import {useParams} from "react-router-dom";

const Invoice = () => {
    const { invoiceId } = useParams();
    const [invoice, setInvoice] = useState({})
    const { user } = UserAuth();

     const getInvoices = async () => {
         console.log("it here",user.uid)
        const i = doc(db, "invoices", invoiceId )
         console.log("dede")
        const querySnapshot = await getDoc(i);
        const data = { id: doc.id, ...querySnapshot.data() }

        console.log(data)
        setInvoice(data)
    };

    useEffect(() => {

        getInvoices()

    }, [])

    return (
        <>
            <Sidebar/>
            <main className="offset-lg-3 ">
                <div className={"border pt-5 rounded-bottom-5 col-lg-8"} style={{backgroundColor:"#6750A4"} }></div>
                <div className={"row pt-5"}>

                    <div className={"col-lg-6"}>
                        <p className={"fw-medium"} >Facture de:
                        <p className={"fs-4 fw-semibold"} style={{color:"#6750A4"}} >{invoice && invoice.customers[0].customerName}</p>
                        <p ><span className={"mt-4 fw-medium"}> Tel: </span></p>{invoice && invoice.customers[1].customerPhone} </p>
                    </div>

                     <div className={"col-lg-6"}>

                        <p className={"fs-4 fw-semibold"} style={{color:"#6750A4"}}>VENDEUR:</p>
                        <p > <span className={"mt-4 fw-medium"}> Nom:</span> Ivana
                        <p className={"mt-1"}> <span className={"mt-4 fw-medium"}> Tel:</span> </p>
                        <p className={"mt-1"}> <span className={"mt-4 fw-medium"}> Date:</span> {convertTimestamp(invoice.timestamp)}</p>

                      </p>

                    </div>
                </div>
                <div className={"col-lg-8 pt-5"}>
                    <table className="table table-responsive table-striped">
                      <thead>
                        <tr className={"table-primary"}>
                          <th scope="col">Désignation</th>
                          <th scope="col">Prix unitaire</th>
                          <th scope="col">Quantité</th>
                          <th scope="col">Montant</th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr >
                          <td>product.productName</td>
                          <td>product.price</td>
                          <td>invoice.productsMerged.quantity</td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>
                </div>
            </main>

        </>
    );

};
export default Invoice;