import {useState} from 'react';
import logo from '../images/logo.png';
import {BsLock, BsPerson, BsTelephone,} from "react-icons/bs";
import {useParams} from 'react-router-dom';
import {db} from '../firebase';
import {doc, setDoc} from "firebase/firestore";
import {useNavigate} from 'react-router-dom';
import {showToast} from '../utils/functions';
import Loading from '../components/Loading';
import {UserAuth} from "../context/AuthContext.jsx";

const Register = () => {
    const navigate = useNavigate();
    const {email} = useParams();
    const [name, setName] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const {createUser} = UserAuth();

    const isPasswordConfirmed = (password, confirmPassword) => {
        if (password && confirmPassword && password === confirmPassword) return true;
        return false;
    }

    const handleSignUp = async (e) => {
        e.preventDefault();
        setError('');

        if (!isPasswordConfirmed(password, confirmPassword)) {
            showToast('error', 'Le mot de passe et le mot de passe de confirmation ne correspondent pas 😭');
            return;
        }

        // Réinitialiser les champs du formulaire
        setName('');
        setPhoneNumber('');
        setPassword('');
        setConfirmPassword('');
        try {
            setLoading(true);
            const credential = await createUser(email, password);
            console.log(credential)
            const usersRef = await setDoc(doc(db, "users", credential.user.uid), {
                id: credential.user.uid,
                name,
                phoneNumber,
                email,
            });
            setLoading(false);
            showToast('success', 'Félicitations vous avez créez votre compte 🚀');
            navigate("/login/" + email);
        } catch (error) {
            setLoading(false);
            showToast('error', 'Une s\'est produite 😭 Réessayez');
            setError(error.message);
            console.log(error.message);
        }
    };


    return (
        <>
            {loading ? (
                <Loading/>
            ) : (
                <main className="d-flex justify-content-center">
                    <div className='col-lg-3'>
                        <form
                            className="pb-5 "
                            onSubmit={handleSignUp}
                        >
                            <div className="">
                                <a href='/'> <img src={logo} alt="Logo" className="img-fluid" style={{width: "200px"}}/>
                                </a>
                            </div>
                            <div className="pb-5">
                                <p className='fs-1' style={{color: "#4F378B"}}> Inscription</p>
                            </div>

                            <label htmlFor="text" className="mb-2 fw-bold" style={{color: "#6750A4"}}>
                                Nom
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsPerson/></i>
                                    <input
                                        type="text"
                                        id="name"
                                        autoComplete='off'
                                        required
                                        className="form-control p-3
             mb-4 border rounded"
                                        placeholder="Entrez votre nom"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="tel" className="mb-2 fw-bold" style={{color: "#6750A4"}}>
                                Numéro de Téléphone
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsTelephone/></i>
                                    <input
                                        autoComplete='off'
                                        id="phoneNumber"
                                        placeholder='Entrez le numéro ex: 655443322'
                                        type="tel"
                                        pattern="^6[0-9]{8}"
                                        className="form-control mb-4 p-3 rounded "
                                        required
                                        value={phoneNumber}
                                        onChange={(e) => setPhoneNumber(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="password" className="mb-2 fw-bold" style={{color: "#6750A4"}}>
                                Mot de passe
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsLock/></i>
                                    <input
                                        id="password"
                                        type="password"
                                        placeholder='Entrez votre mot de passe'
                                        className="col-lg-12 mb-4 border p-3 rounded "
                                        required
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </div>
                            </div>

                            <label htmlFor="confirmPassword" className="mb-2 fw-bold" style={{color: "#6750A4"}}>
                                Confirmer votre mot de passe
                            </label>
                            <div>
                                <div className="input">
                                    <i className='p-3 '> <BsLock/></i>
                                    <input
                                        id="confirmPassword"
                                        type="password"
                                        placeholder='Entrez votre mot de passe à nouveau'
                                        className="col-lg-12 mb-4 border p-3 rounded "
                                        required
                                        value={confirmPassword}
                                        onChange={(e) => setConfirmPassword(e.target.value)}
                                    />
                                </div>
                            </div>
                            <button
                                type="submit"
                                className="col-lg-12 p-3 text-white btn btn-lg mt-5 "
                                style={{backgroundColor: "#1E1A76"}}>
                                Inscription
                            </button>

                        </form>
                    </div>
                </main>
            )}
        </>
    );
};

export default Register; 